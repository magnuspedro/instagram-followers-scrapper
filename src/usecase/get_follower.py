from src.entities.enum.resources import Resources
from src.gateway.instagram.follwer_converter import FollowerConverter
from src.gateway.instagram.instagram import Instagram
from src.config.logger.logging_module import PTLogger

logger = PTLogger(name=__name__)


class GetFollower:
    def execute(self):
        profiles = []

        for nick_name in Resources.NICKNAMES.value:
            instagram_page = Instagram.getPage(f'{Resources.URL.value}{nick_name}')
            logger.info('Converting HTML to Entity')
            profile = FollowerConverter().convertToEntity(instagram_page)
            logger.info('Converted successfully')
            logger.info(profile)
            profiles.append(profile)
        return profiles