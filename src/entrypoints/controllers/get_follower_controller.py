from flask_api import FlaskAPI
from flask import Blueprint
from src.usecase.get_follower import GetFollower

followers = Blueprint('followers', __name__)


@followers.route('/followers', methods=['GET'])
def getFollowers():
    return GetFollower().execute()


