from concurrent.futures import ThreadPoolExecutor

from src.usecase.get_follower import GetFollower


def main():
    run_parallel_usecase(
        lambda: GetFollower().execute()
    )


def run_parallel_usecase(*tasks):
    with ThreadPoolExecutor() as executor:
        running_tasks = [executor.submit(task) for task in tasks]
        for running_task in running_tasks:
            running_task.result()
