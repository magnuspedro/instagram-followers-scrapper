from src.entrypoints.controllers.get_follower_controller import followers
from flask_api import FlaskAPI

app = FlaskAPI(__name__)
app.register_blueprint(followers)

def run(): 
    app.run()