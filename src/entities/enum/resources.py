from enum import Enum


class Resources(Enum):
    NICKNAMES = ['fiuk','kercardoso','juliette.freire','arthurpicoli',
    'afiune_caio','joaolpedrosa','bilaraujjo','carladiaz','karolconka','camilladelucas',
    'pocah','negodioficial','lucaskokapenteado','irodolffo','lumena.aleluia','gilnogueiraofc','viihtube','thaisfbraz','projota','sarah_andrade']
    URL = 'https://www.instagram.com/'
