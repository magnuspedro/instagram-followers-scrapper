from dataclasses import dataclass

@dataclass
class Profile:
    nick_name: str
    followers: int
    following: int