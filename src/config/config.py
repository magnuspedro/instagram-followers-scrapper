from enum import Enum


class Config(Enum):
    REQUEST_RETRY = 10
    WAITING_TIME = 2