from src.gateway.request import Request


class Instagram:

    @staticmethod
    def getPage(url):
        response = Request(url=url).request()
        return response
