from bs4 import BeautifulSoup
from url_parser import get_url
from url_parser import get_url

import re
import json

from src.entities.profile import Profile


class FollowerConverter:

    def convertToEntity(self, response):
        soup = BeautifulSoup(response.content, 'html.parser')
        script_tag = soup.findAll('script')
        followers = re.search('"edge_followed_by":{"count":[0-9]+}', str(script_tag))
        followers = json.loads('{' + followers.group(0) + '}')
        followers_number = followers['edge_followed_by']['count']
        nick_name = get_url(response.url).path.strip('/')

        return Profile(nick_name=nick_name, followers=followers_number, following=None)
