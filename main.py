from src.entrypoints.command_line import main
from src.entrypoints.controller_executer import run


if __name__ == "__main__":
    run()
