# Instagram Followers Scrapper

## Dependency installation
 - install pipenv
 - run: pipenv shell
 - run: pipenv install

## Running script 

 - run: pipenv shell
 - run: python main.py
 
 ## To add more usernames
 
 Go to src/entities/enum/resources and add the username to the array.